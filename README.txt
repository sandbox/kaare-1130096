
Turn user account operations (login, create, recover) into a popup window using
jQuery UI.

The popup window will have tabs between these operations, loaded only on request
using ajax.  Error messages and cross-links between these operations is all done
within the dialog.  It's also possible to configure whether the user should be
redirected to its profile page or just reload the current page, based on current
path.  The ?destination= GET argument is properly honored.


REQUIREMENTS

jquery_ui module must have jQuery UI 1.7.X installed and therefore is
jquery_update-6.x-2.x also required.  A theme for jQuery UI must be working,
even though there is no simple way to add this.  This can either be added to
your site theme, or patching jquery_ui using the latest patch in this issue:

	 http://drupal.org/node/388384


INSTALL

Enable the module in your favourite way:

       $ drush -y en jquser

Configure the settings for this in admin/user/jquser.  Create any links to
user/login, user/register or user/recover, and any of this will be presented as
a jquery_ui popup once clicked.
