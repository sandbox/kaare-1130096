
Drupal.jquser = Drupal.jquser || {};

/**
 * Find the ?destionation=target GET parameter in uri
 */
Drupal.jquser.findDestination = function(uri) {
    var hit = uri.match(/[&?]destination=([^\s&?]+)/);
    return hit ? hit[1] : null;
};

/**
 * Connect user links to their respective popup tabs
 */
Drupal.jquser.bindLink = function(context, action) {
    $('a[href*="'+ Drupal.settings.basePath +'user/'+ action +'"]', context).click(function() {
	var dest = Drupal.jquser.findDestination(this.href);
	if (dest) {
	    var new_uri = Drupal.settings.basePath + 'jquser/get_form/user_' + action + '?destination=' + dest;
	    Drupal.jquser.tabs.tabs('url', Drupal.jquser.index[action], new_uri);
	}
	if (Drupal.jquser.tabs.tabs('option', 'selected') >= 0) {
	    Drupal.jquser.dialog.dialog('open');
	}
	Drupal.jquser.tabs.tabs('option', 'selected', Drupal.jquser.index[action]);
	return false;
    });
};

/**
 * Prepare a retrieved form for ajax submission
 */
Drupal.jquser.prepareForm = function(context) {
    // Hackety hack coming up ...
    // Se comment in jquser.module in function jquser_get_form()
    if ($(context).html().substring(0, 7) == '!nohtml') {
	var response = $(context).html().split(':');
	if (response[1] == 'logged-in') {
	    window.location.href = Drupal.settings.basePath + (response.length > 2 ? response[2] : Drupal.settings.jquser.q);
	}
	return false;
    }
    $(context).find('form').ajaxForm({
	dataType: 'json',
	success: function(response) {
	    $(context).html(response.display);
	    if (! response.status) {
		$(context).html(response.display);
		Drupal.jquser.prepareForm(context);
	    } else {
		$(context).find('form input').attr('disabled', 'disabled');
		window.location.href = Drupal.settings.basePath + (response.destination ? response.destination : Drupal.settings.jquser.q);
	    }
	}
    }).find('input[name="jquser_q"]').attr('value', Drupal.settings.jquser.q);
    for (var idx in Drupal.jquser.index) {
	Drupal.jquser.bindLink(context, idx);
    }
    return true;
};

Drupal.behaviors.jquser = function(context) {
    var $body = $('body');
    if ($body.hasClass('logged-in') || $body.hasClass('jquser-processed')) {
	return;
    }
    $body.addClass('jquser-processed');

    var ajax_path = Drupal.settings.basePath + 'jquser/get_form';
    var dialog = $('<div class="jquser-user">');
    var tabindex = 0;
    var ul = $('<ul />')
	.append('<li><a href="'+ ajax_path +'/user_login"><span>'+ Drupal.t('Log in') +'</span></a></li>');
    Drupal.jquser.index = { login: tabindex++ };
    if (Drupal.settings.jquser.user_register > 0) {
	ul.append('<li><a href="'+ ajax_path +'/user_register"><span>'+ Drupal.t('Create new account') +'</span></a></li>');
	Drupal.jquser.index.register = tabindex++;
    }
    ul.append('<li><a href="'+ ajax_path +'/user_pass"><span>'+ Drupal.t('Request new password') +'</span></a></li>');
    Drupal.jquser.index.password = tabindex++;
    var tabs = $('<div id="jquser-tabs" />').append(ul);
    dialog.append(tabs);
    tabs.tabs({
	cache: true,
	selected: -1,
	load: function(event, ui) {
	    if (Drupal.jquser.prepareForm(ui.panel)) {
		dialog.dialog('open');
	    }
	},
	show: function(event, ui) {
	    $($('input[type="text"]', ui.panel)[0]).focus();
	}
    });
    dialog.dialog(Drupal.settings.jquser.dialog);

    Drupal.jquser.dialog = dialog;
    Drupal.jquser.tabs = tabs;

    for (var action in Drupal.jquser.index) {
	Drupal.jquser.bindLink(context, action);
    }
};
