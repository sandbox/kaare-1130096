<?php

function jquser_admin_settings() {
  $form['redirection'] = array(
    '#type' => 'fieldset',
    '#title' => t('User redirection'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#group' => 'jquser_settings',
  );
  $form['redirection']['jquser_redirect'] = array(
    '#type' => 'radios',
    '#title' => t("How to redirect the user upon successful login"),
    '#options' => array(
      'current'      => "Reload current page",
      'user'         => "To the user's profile",
      'user_on_page' => "To the user's profile if on the listed pages, otherwise reload",
      'user_no_page' => "To the user's profile if <em>not</em> on the listed pages, otherwise reload",
    ),
    '#default_value' => variable_get('jquser_redirect', 'current'),
    '#description' => "",
  );
  $form['redirection']['jquser_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('jquser_pages', ''),
    '#description' => t("Enter one page per line as Drupal paths. The '<code>*</code>' character is a wildcard. <code>&lt;front&gt;</code> is the front page"),
  );

  $dialog = _jquser_dialog();

  $form['jquser_dialog'] = array(
    '#type'  => 'fieldset',
    '#title' => t("jQuery UI dialog settings"),
    '#collapsible' => true,
    '#collapsed' => true,
    '#tree' => true,
    '#group' => 'jquser_settings',
  );
  $form['jquser_dialog']['title'] = array(
    '#type' => 'textfield',
    '#title' => t("Title"),
    '#description' => t("Specifies the title of the dialog. Any valid HTML may be set as the title."),
    '#default_value' => $dialog['title'],
  );
  $form['jquser_dialog']['width'] = array(
    '#type' => 'textfield',
    '#title' => t("Width"),
    '#description' => t("The height of the dialog, in pixels. Specifying 'auto' is also supported to make the dialog adjust based on its content."),
    '#size' => 10,
    '#default_value' => $dialog['width'],
  );
  $form['jquser_dialog']['height'] = array(
    '#type' => 'textfield',
    '#title' => t("Height"),
    '#description' => t("The height of the dialog, in pixels. Specifying 'auto' is also supported to make the dialog adjust based on its content."),
    '#size' => 10,
    '#default_value' => $dialog['height'],
  );
  $form['jquser_dialog']['minWidth'] = array(
    '#type' => 'textfield',
    '#title' => t("Minimum width"),
    '#size' => 10,
    '#default_value' => $dialog['minWidth'],
    '#description' => t("The minimum width to which the dialog can be resized, in pixels."),
  );
  $form['jquser_dialog']['minHeight'] = array(
    '#type' => 'textfield',
    '#title' => t("Minimum height"),
    '#description' => t("The minimum height to which the dialog can be resized, in pixels."),
    '#size' => 10,
    '#default_value' => $dialog['minHeight'],
  );
  $form['jquser_dialog']['modal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Modal'),
    '#default_value' => $dialog['modal'],
    '#description' => t("If set to true, the dialog will have modal behavior; other items on the page will be disabled (i.e. cannot be interacted with). Modal dialogs create an overlay below the dialog but above other page elements."),
  );
  $form['jquser_dialog']['draggable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Draggable'),
    '#default_value' => $dialog['draggable'],
    '#description' => t("If set to true, the dialog will be draggable will be draggable by the titlebar."),
  );
  $form['jquser_dialog']['resizable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Resizable'),
    '#default_value' => $dialog['resizable'],
    '#description' => t("If set to true, the dialog will be resizeable."),
  );

  if (module_exists('vertical_tabs')) {
    $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
  }
  return system_settings_form($form);
}

