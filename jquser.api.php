<?php

/**
 * Modify jquery UI dialog options for jQuery User login window.  See
 * http://jqueryui.com/demos/dialog/ for documentation.
 *
 * @param $dialog
 *   The dialog settings array as it will be handed over to the jquery UI's
 *   dialog() method.
 */
function hook_jquser_dialog_alter(&$dialog) {
  // Set position of dialog to 100x100 px from top left corner.
  $dialog['position'] = array(100, 100);
}
